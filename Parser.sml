signature GRAMMAR =
sig

    type Alphabets
    type States
    type Rule
    datatype Symbols = Terminal of string
		 | NonTerminal of string
    type Grammar
    val Compute : Grammar
                  -> (string * {FI:string list, FO:string list, N:bool}) list
									 
    val CreateLL1Table : (string * {FI:string list, FO:string list, N:bool})
                           list
                         -> Grammar
                         -> (string * string list) list Array2.array

    val IsLL1 : 'a list Array2.array -> bool
    val Closure : (string * string list) list
                  -> 'a * 'b * 'c * Rule list -> (string * string list) list

end
structure Grammar : GRAMMAR =
struct
type Alphabets = string list 				     
type States = string list

datatype Symbols = Terminal of string
		 | NonTerminal of string	  
type Rule = string * Symbols list
type StartState = string
type Grammar = StartState * Alphabets * States *  Rule list 
fun Find [] _ = NONE
  | Find ((x,z)::xs) (y:string) = if x=y then SOME z else (Find xs y);

fun IsSame [] Y = true
  | IsSame (x::xs) Y = (List.exists (fn y => x=y) Y) andalso IsSame xs Y;
fun RemDuplicate []  = [] 
  | RemDuplicate (x::xs) = x::RemDuplicate (List.filter (fn y => y<>x) xs);
						
fun Union X Y = RemDuplicate (X @ Y);

fun Split X i j = if i>j orelse (j>= length X) then [] else List.nth (X,i)::Split X (i+1) j ;

fun UpdateFO ({FO = _, FI, N}, a) = {FO = a, FI = FI, N = N};
fun UpdateFI ({FO, FI = _, N}, b) = {FO = FO, FI = b, N = N};
fun UpdateN ({FO, FI, N}, c) = {FO = FO, FI = FI, N = c orelse N};

fun Initalise alp x = if List.exists (fn y => x=y) alp 
		      then (x, {FO = []:string list , FI = [x]:string list ,N = false})
		      else (x, {FO = []:string list , FI = []:string list ,N = false });
 
fun Nullable (Nterm, []) _ =  true
  | Nullable (NTerm, (x::xs)) sym = 
    let
	val {FO ,FI , N} = valOf (Find sym x)
    in
	N andalso Nullable (NTerm, xs) sym
    end;
fun First sym slist NTerm Y = 
  let
      val {FO ,FI , N} = valOf (Find sym NTerm)
      val firstNT = FI;
      val {FO ,FI , N} = valOf (Find sym Y)
  in
      if Nullable (NTerm, slist) sym
      then Union firstNT FI
      else firstNT
  end;
fun Follow1 sym slist Y X =
  let
      val {FO ,FI , N} = valOf (Find sym Y)
      val tFO = FO;
      val {FO ,FI , N} = valOf (Find sym X)
  in
      if Nullable (X, slist) sym
      then Union tFO FO
      else tFO
  end
      
fun Follow2 sym slist Y X =
  let
      val {FO ,FI , N} = valOf (Find sym Y)
      val tFO = FO;
      val {FO ,FI , N} = valOf (Find sym X)
  in
      if Nullable (X, slist) sym
      then Union tFO FI
      else tFO
  end
fun ComputePerProd [] sym = sym
  | ComputePerProd ((s,l)::xs) sym =
    let
	val z  = ref (valOf (Find sym s));
	val symb = ref ((s , UpdateN((!z), Nullable (s,l) sym))::(List.filter (fn (a,b) => a<>s) sym));
	val i = ref 1;
	val j = ref 2;
	val k = length l;
	val FI = ref ["a"];
	val yi_sym = ref s;
	val yi = ref (!z);
	val yj = ref (!z);
	val yj_sym = ref s;
	val FO = ref ["a"];
    in
	while !i <= k do (
	    j:= !i + 1;
	    z := valOf (Find (!symb) s);
	    yi_sym := List.nth(l, (!i-1));
	    FI := First (!symb) (Split l 0 (!i-2)) s (!yi_sym) ;
	    symb := ((s,UpdateFI((!z),(!FI)))::(List.filter (fn (a,b) => a<>s) (!symb)));
	    
	    yi := valOf (Find (!symb) (!yi_sym));
	    z := valOf (Find (!symb) (s));
	    FO := Follow1 (!symb) (Split l (!i) (k-1)) (!yi_sym) s;
	    symb := (((!yi_sym), UpdateFO((!yi),(!FO)))::(List.filter (fn (a,b) => a<>(!yi_sym)) (!symb)));
		
	    while !j <= k do (
		yi := valOf (Find (!symb) (!yi_sym));
		yj_sym := List.nth(l, (!j-1));
		yj := valOf (Find (!symb) (!yj_sym));
		FO := Follow2 (!symb) (Split l (!i) (!j - 2)) (!yi_sym) (!yj_sym) ;
		symb := (((!yi_sym), UpdateFO((!yi), (!FO)))::(List.filter (fn (a,b) => a<>(!yi_sym)) (!symb)));
		j := (!j) + 1
	    );
	    i := (!i) + 1
	);
	ComputePerProd xs (!symb)		     
    end
fun RemoveTags [] = []
  | RemoveTags (Terminal(x)::xs) = x::RemoveTags xs
  | RemoveTags (NonTerminal(x)::xs) = x::RemoveTags xs;
 
fun PreProcessGrammer (start,alpha,states,(rules:Rule list)) = (start, alpha, states, (map (fn (x,y) => (x, RemoveTags y)) rules))
								   
fun Compute (G:Grammar) =
  let
      val (_,alphab,states,rules) = PreProcessGrammer G
      val symbols = map (Initalise alphab) (states @ alphab)	    	      
      fun Repeat rules sym =
	let
	    val sym_new = ComputePerProd rules sym;
	in
	    if IsSame sym sym_new
	    then sym
	    else Repeat rules sym_new
	end
  in
      Repeat rules symbols
  end

fun FirstOfList symb nterm [] =
  let
      val {FO=f, FI, N} = valOf (Find symb nterm);
  in
      f
  end    
  | FirstOfList symb nterm (x::xs) = 
    let
	val {FO, FI=f, N} = valOf (Find symb x);
    in
	Union f (if N then FirstOfList symb nterm xs else [])
    end
	
fun IsElement t slist = List.exists (fn y => t=y) slist;
fun FindIndex [] t = NONE
  | FindIndex (x::xs) t = if x=t
			  then SOME 1
			  else SOME (1 + valOf (FindIndex xs t));

fun CreateLL1Table symb (G:Grammar) =
  let
      val (start,alphab,states,rules) = PreProcessGrammer G;
      val table = Array2.array (length states, length alphab, []);
      fun UpdateLL1Table symb [] table = table
	| UpdateLL1Table symb ((s,l)::xs) table = 
	let
	    val i = valOf (FindIndex states s);
	    fun tmfun x = valOf (FindIndex alphab x)
	    val j = map (tmfun) (FirstOfList symb s l)
	    fun update (s,l) table i [] = table
	      | update (s,l) table i (j::js) = 
	      let
		  val ele = Array2.sub(table, i-1, j-1);
	      in
		  Array2.update (table, i-1, j-1 ,(Union ele [(s,l)]));
		  update (s,l) table i js 
	      end
	    val tabl = update (s,l) table i j
	in
	    UpdateLL1Table symb xs tabl
	end
  in
      UpdateLL1Table symb rules table
  end
fun CheckLL1 table 0 0 = 1 >= length (Array2.sub(table, 0, 0))
  | CheckLL1 table 0 n = (1 >= length (Array2.sub(table, 0, n))) andalso (CheckLL1 table ((Array2.nRows(table))-1) (n-1))
  | CheckLL1 table m n = (1 >= length (Array2.sub(table, m, n))) andalso (CheckLL1 table (m-1) (n))
fun IsLL1 table = (CheckLL1 table ((Array2.nRows(table))-1) ((Array2.nCols(table))-1));      

fun FindProdFromX ((s,l)::xs) x = if s=x then ((s,l)::FindProdFromX xs x) else (FindProdFromX xs x)
  | FindProdFromX [] x  = nil ;
fun AddProduItems ((s,l)::xs) x =
  let
      val ele = (s,("."::l))
  in
      if IsElement ele x
      then AddProduItems xs x
      else ele::AddProduItems xs x
  end
  | AddProduItems [] _ = nil ;

fun PerLRItem ((s,l)::Is) rules =
  let
      val index = valOf (FindIndex l ".");
      val X = if index < length l then SOME (List.nth(l, index )) else NONE;
      val produ = if X=NONE then [] else FindProdFromX rules (valOf X) ;
      val I_new = Union Is (AddProduItems produ ((s,l)::Is))
  in
      ((s,l)::PerLRItem I_new rules)
  end
  | PerLRItem [] _ = nil ; 
      
fun Closure I G =
  let
      val (start,alphab,states,rules) = PreProcessGrammer G;
      fun Repeat I rules =
	let
	    val old_I = I;
	    val I = PerLRItem I rules
	in
	    if (IsSame I old_I) andalso (length I = length old_I)
	    then I
	    else Repeat I rules
	end
  in
      Repeat I rules
  end

end

val G:Grammar.Grammar = ("S", ["a","b"], ["S", "A", "B"], [("S", [Grammar.NonTerminal("A"), Grammar.NonTerminal("B")]), ("A", []), ("A", [Grammar.Terminal("a")]), ("B", [Grammar.Terminal("b")]), ("B", [])])
val Test:Grammar.Grammar = ("S",
			    ["(",")","*","+","-","/","id","num","End"],
			    ["S", "E", "E_","T","T_","F"],
			    [("S", [Grammar.NonTerminal("E"), Grammar.NonTerminal("End")]),
			     ("E", [Grammar.NonTerminal("T"), Grammar.NonTerminal("E_")]),
			     ("E_", []),
			     ("E_", [Grammar.Terminal("+"), Grammar.NonTerminal("T"), Grammar.NonTerminal("E_")]),
			     ("E_", [Grammar.Terminal("-"), Grammar.NonTerminal("T"), Grammar.NonTerminal("E_")]),
			     ("T", [Grammar.NonTerminal("F"), Grammar.NonTerminal("T_")]),
			     ("T_", [Grammar.Terminal("*"), Grammar.NonTerminal("F"), Grammar.NonTerminal("T_")]),
			     ("T_", [Grammar.Terminal("/"), Grammar.NonTerminal("F"), Grammar.NonTerminal("T_")]),
			     ("T_", []),
			     ("F", [Grammar.Terminal("id")]),
			     ("F", [Grammar.Terminal("num")]),
			     ("F", [Grammar.Terminal("("),Grammar.NonTerminal("E"),Grammar.Terminal(")")])])
val x = Control.Print.printLength := 500;
val Result = Grammar.Compute Test;



val table = Grammar.CreateLL1Table Result Test;

val LL1 = Grammar.IsLL1 table 
val I = Grammar.Closure [("S",[".", "E", "End"])] Test; 
