use "basic_block.sml";
signature Instruction =
sig
    eqtype inst
    val useSet : inst -> AtomSet.set (* Get the use set of an instruction *)
    val defSet : inst -> AtomSet.set (* Get the def set of an instruction *)
end

(* Functor to make Function which will find INOUT Sets Assuming Given Graph is having Instruction as Node *)
functor Livenessfn (I:Instruction) =
struct
structure NodeMap = ListMapFn (struct
				    type ord_key = int
				    val compare = Int.compare
				    end)
local
    structure X = Graph(type node = I.inst)
    (* Initalize or Create In and Out Sets with empty atomSet *)
    fun Initalize [] _ = NodeMap.empty (* Should start with c=1 *)
      | Initalize (n::ns) c = NodeMap.insert (Initalize ns (c+1), c, AtomSet.empty)
    (* Finding Index of n in an array(list) *)
    fun FindIndex [] _ = 0
      | FindIndex (x::xs) n = if x=n then 1 else 1 + (FindIndex xs n)
    (* Find InSet from  node n and OutSet of n in previous run (In(n) = useSet(n) U (Out(n) - def(n)) ) *)
    fun InSet n Outn = AtomSet.union (I.useSet n, AtomSet.difference (Outn, I.defSet n))
    (* Helper function to find list of In set for all succ nodes *)
    fun FindINs (ns:X.node list) M_in succ= List.map (fn x => NodeMap.lookup (M_in, FindIndex ns x)) succ
    (* Find OutSet from  node n (Out(n) = (Union of all IN(x) where x in a succ of n )  *)
    fun OutSet ins G n M_in = List.foldr AtomSet.union AtomSet.empty (FindINs ins M_in (X.suc G n))
    (*Checking Is AtomSet list is same or not*)
    fun IsSameAtomSetList (x::xs) (y::ys) = AtomSet.equal(y,x) andalso  IsSameAtomSetList xs ys
      | IsSameAtomSetList [] [] = true
      | IsSameAtomSetList _ _ = false
    (* Check weather x and y are same map or not *)
    fun IsSameMap x y =
      let
	  val Xs = NodeMap.listItems x
	  val Ys = NodeMap.listItems y
      in
	  IsSameAtomSetList Xs Ys
      end
	  
in
    (*Graph Structure for given Instruction Format*)
open X
    (*Finds In Sets and Out Sets for a Graph*)
    fun INOUTSets (G:X.graph) =
      let
	  val N = X.nodes G 	                                     (* Find Nodes from Graph *)
	  val M_in = Initalize N 1                                   (* Initalize InSets to Empty *)
	  val M_out = Initalize N 1                                  (* Initalize OutSets to Empty *)
	  (* Done for Each node until M_in and M_out doesnot change*)
	  fun repeatuntil (n::ns) M_in M_out =
	    let
		val index = FindIndex N n
		val In = InSet n (NodeMap.lookup (M_out, index))    (* Finding In(n) = useSet(n) U (Out(n) - def(n)) *)   
		val Out = OutSet N G n M_in                         (* (Out(n) = (Union of all IN(x) where x in a succ of n )  *)
		val nM_in = NodeMap.insert (M_in, index, In)        (* Update in and out set for node n *)
		val nM_out = NodeMap.insert (M_out, index, Out)
	    in
		repeatuntil ns nM_in nM_out
	    end
	    | repeatuntil [] M_in M_out = (M_in, M_out)
	  (* Function which calls itself until M_in and M_out does not change*)
	  fun repeat M_in M_out =
	    let
		val (nM_in, nM_out) = repeatuntil N M_in M_out 
	    in
		if (IsSameMap nM_in M_in) andalso (IsSameMap nM_out M_out)
		then (M_in, nM_out)
		else repeat nM_in nM_out
	    end
      in
	  repeat M_in M_out
      end
end
end
(* Functor To Make Functions required for making and processing Basic Block Graphs*)
functor BasicBlockGraph (I:Instruction) =
struct
local
    structure X = Graph(type node = I.inst)
    structure Ba = Blockfn (X)
    open Ba
    structure BasicB = Graph(type node = X.node list)
    fun RemDuplicate []  = [] 
      | RemDuplicate (x::xs) = x::RemDuplicate (List.filter (fn y => y<>x) xs);
    open BasicB
in
    (* GenSet Which takes a list of Instructions (List Capture Basic Block) *)
    fun GenSet (n::nil) = I.useSet n                                                                 (* Genset(n::ns)  = Use(n) U (Gen(ns) - def(n)) *)
      | GenSet (n::ns) = AtomSet.union (I.useSet n, AtomSet.difference (GenSet ns, I.defSet n))
      | GenSet [] = AtomSet.empty
    (* KillSet Which takes a list of Instructions (List Capture Basic Block) *)
    fun KillSet (N:I.inst list) = List.foldr AtomSet.union AtomSet.empty (List.map I.defSet n)        (* Kill(block) = Union over all nodes in block def(n)  *)
    local 
    structure Live_BasicBlock = Livenessfn(struct
					    type inst = node
					    val useSet = GenSet
					    val defSet = KillSet
					    end)
    in
    (* Structure for Graph and INOUT Sets Computation in BasicBlock Graph *)
       open Live_BasicBlock
    end
     (* Function To Convert Graph into Basic BlockGraph *)
    fun CreateBasicBlockGraph (G:X.graph):graph =
      let
	  val B = DetectBasicBlocks G
	  val E = X.edges G
	  fun Find (n::ns) x = if (List.exists (fn y => y=x) n) then SOME n else Find ns x
	    | Find [] _ = NONE
	  fun Ppred (n::ns) s = (valOf (Find B n), s)::Ppred ns s
	    | Ppred [] s = nil
	  fun Convert (b::bs) = (Ppred (X.pred G (List.hd b)) b) @ (Convert bs)
	    | Convert [] = nil
      in
	  (ref B, ref (RemDuplicate (Convert B)))
      end
end
end
(*Test For Liveness*)
structure I =
struct
type use = string list
type def = string list
type instru = string
datatype inst = Inst of instru * use * def
fun useSet (Inst (_,u,_)) = AtomSet.fromList (List.map Atom.atom u)
fun defSet (Inst (_,_,d)) = AtomSet.fromList (List.map Atom.atom d)
end
    
structure L = Livenessfn (I);
structure B = BasicBlockGraph(I);
(*Making Program Graph*)
val (PG:L.graph) = (ref [], ref []);
L.newNode PG (I.Inst("a:=0", [],["a"]));
L.newNode PG (I.Inst("L: b:=a+1", ["a"],["b"]));
L.newNode PG (I.Inst("c:=c+b", ["b","c"],["c"]));
L.newNode PG (I.Inst("a:=b*2", ["b"],["a"]));
L.newNode PG (I.Inst("if a< N goto L", ["a"],[]));
L.newNode PG (I.Inst("return c", ["c"],[]));
L.addEdge (I.Inst("a:=0", [],["a"]), I.Inst("L: b:=a+1", ["a"],["b"])) PG;
L.addEdge (I.Inst("L: b:=a+1", ["a"],["b"]), I.Inst("c:=c+b", ["b","c"],["c"])) PG;
L.addEdge (I.Inst("c:=c+b", ["b","c"],["c"]), I.Inst("a:=b*2", ["b"],["a"])) PG;
L.addEdge (I.Inst("a:=b*2", ["b"],["a"]), I.Inst("if a< N goto L", ["a"],[])) PG;
L.addEdge (I.Inst("if a< N goto L", ["a"],[]), I.Inst("L: b:=a+1", ["a"],["b"])) PG;
L.addEdge (I.Inst("if a< N goto L", ["a"],[]), I.Inst("return c", ["c"],[])) PG;
(*Calculating IN set and Out sets for each node in Program graph*)
val (I,O) = L.INOUTSets PG;
val OUT = L.NodeMap.listItems O;
val IN = L.NodeMap.listItems I;
val O = List.map AtomSet.listItems OUT
val o1 = List.map Atom.toString (List.nth(O,0));
val o2 = List.map Atom.toString (List.nth(O,1));
val o3 = List.map Atom.toString (List.nth(O,2));
val o4 = List.map Atom.toString (List.nth(O,3));
val o5 = List.map Atom.toString (List.nth(O,4));
val o6 = List.map Atom.toString (List.nth(O,5));
val I = List.map AtomSet.listItems IN
val i1 = List.map Atom.toString (List.nth(I,0));
val i2 = List.map Atom.toString (List.nth(I,1));
val i3 = List.map Atom.toString (List.nth(I,2));
val i4 = List.map Atom.toString (List.nth(I,3));
val i5 = List.map Atom.toString (List.nth(I,4));
val i6 = List.map Atom.toString (List.nth(I,5));
(*Capturing Basic Block and finding In Out Sets for each Basic Block in Program Graph *)
val BG = B.CreateBasicBlockGraph PG;			     
val (I,O) = B.INOUTSets BG;
val OUT = B.NodeMap.listItems O;
val IN = B.NodeMap.listItems I;
val O = List.map AtomSet.listItems OUT
val o1 = List.map Atom.toString (List.nth(O,0));
val o2 = List.map Atom.toString (List.nth(O,1));
val o3 = List.map Atom.toString (List.nth(O,2));
val I = List.map AtomSet.listItems IN
val i1 = List.map Atom.toString (List.nth(I,0));
val i2 = List.map Atom.toString (List.nth(I,1));
val i3 = List.map Atom.toString (List.nth(I,2));
