(*
lab Assignment 0
*)
structure Binary_Tree =
struct

datatype 'a Tree = Null
	    	 | Node of 'a Tree * 'a * 'a Tree

fun inorder Null = []
  | inorder (Node(left, key, right)) = inorder left @ key :: inorder right ;


fun anticlockwise (Node(left, key, Node(rleft, rkey, rright))) = (Node(Node(left, key, rleft), rkey, rright))
  | anticlockwise x = x;

end
