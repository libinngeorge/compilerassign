(*
ASSIGNMENT 1
*)
signature ORD = 
sig

type t

val le : t -> t -> bool

end
    
structure IntOrd : ORD =
struct
(*
type is used to give new name 't' for existing type int
*)
type t = int;

fun le x y  = x<=y;

end
    
structure RealOrd : ORD =
struct
(*
type is used to give new name 't' for existing type real
*)
type t = real;

fun le (x:t) (y:t) = (x<=y);

end
(*
Functor is used to create a structure from other structure/s
struct is like variable 
and signature is like Detatypes (ex: int)
*)
functor Qsort (X : ORD) = struct

fun QuickSort [] = []
  | QuickSort (x :: xs) =
    let
	val (s,b) = List . partition (X . le x) xs
    in
	QuickSort s @ [x] @ QuickSort b
    end;

end;

(*
testing the above structures and functors
*)
structure IntSort = Qsort(IntOrd);
structure RealSort = Qsort(RealOrd);
IntSort.QuickSort [1,4,7,9,4,2,10,5,7,4];
RealSort.QuickSort [1.2, 7.4, 3.4, 8.6, 4.6, 6.6, 12.56, 12.34, 4.5, 10.1];

