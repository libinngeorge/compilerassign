
signature Graph = sig
   eqtype node
   type   graph     (* mutable variant of a graph *)

   val newNode : graph -> node          -> unit
   val addEdge : (node*node) -> graph   -> unit
   val nodes   : graph       -> node list
   val edges   : graph       -> (node*node) list
   val suc     : graph->node -> node list
   val pred    : graph->node -> node list

end
		      
(* Functor to Make Graph data Structure from given node Type*)
functor Graph (X : sig eqtype node end) :Graph = struct
open X
type edge = node * node
type graph = (node list ref * (node * node) list ref)
		 
fun newNode ((N,E):graph) (n:node) = N:=(n::(!N))
					    
fun addEdge (e:node*node) ((N,E):graph) = E:=(e::(!E))
						 
fun nodes (((N:node list ref ,E:edge list ref)):graph) = !N
fun edges (((N:node list ref ,E:edge list ref)):graph) = !E	     
fun suc ((N,ref ((n1,n2)::es)):graph) (n:node) = if n1=n
						 then n2::(suc (N, ref es) n)
						 else suc (N, ref es) n
  | suc ((N,ref []):graph) _ = []
				   
fun pred ((N,ref ((n1,n2)::es)):graph) (n:node) = if n2=n
						  then n1::(pred (N, ref es) n)
						  else pred (N, ref es) n
  | pred ((N,ref []):graph) _ = [] 
end
(* Funtor to Make Function to Detect Basic Block*)
functor Blockfn (X:Graph) = struct
local 
    structure SetNode = RedBlackSetFn (struct
					type ord_key = int
					val compare = Int.compare
					end)
    structure NodeMap = ListMapFn (struct
				    type ord_key = int
				    val compare = Int.compare
				    end)
				  
    structure IntNode = struct
    type node = int
    end
    structure IntGraph = Graph (IntNode)
    open IntGraph				  
    fun ConvertGraphtoInt (G:X.graph) =
      let
	  val V = X.nodes G
	  val E = X.edges G
	  fun Indexify [] _ = []
	    | Indexify (n::ns) c = (n,c)::Indexify ns (c+1)
	  fun Createmap [] = NodeMap.empty
	    | Createmap ((n, c)::ncs) = NodeMap.insert ((Createmap ncs), c, n)
	  fun FindIndex [] _= NONE
	    | FindIndex ((n,c)::ncs) v = if n=v then SOME c else FindIndex ncs v
	  fun CreateEdges [] _ = [] 
	    | CreateEdges ((n1, n2)::es) Vindex = (valOf (FindIndex Vindex n1) , valOf (FindIndex Vindex n2))::CreateEdges es Vindex
      in
	  let
	      val Vindex = Indexify V 0
	      val Vmap = Createmap Vindex
	      val Edges = CreateEdges E Vindex
	      val (IntG:IntGraph.graph) = (ref (NodeMap.listKeys Vmap), ref (Edges))
	  in
	      (IntG,Vmap)
	  end
      end 
in
(* Function to Detect Basic Blocks Returns list containing Basic block(list of nodes)*)
fun DetectBasicBlocks (G:X.graph) =
  let
      val (Gnew, Vmap) = ConvertGraphtoInt G
      val Nodes = nodes Gnew
      (* Function to Find nodes which will Start basic Blocks*)
      fun initStarters [] _ = SetNode.empty
	| initStarters ((n::ns):node list) Gnew = if length(pred Gnew n)<>1
					       then (SetNode.add ((initStarters ns Gnew), n))
						  else (initStarters ns Gnew)
      (* Function to Find nodes which will end basic Blocks*)
      fun initEnders [] _ = SetNode.empty
	| initEnders ((n::ns):node list) Gnew = if length(suc Gnew n)<>1
					     then (SetNode.add ((initEnders ns Gnew),n))
					     else (initEnders ns Gnew)
      val st = initStarters Nodes Gnew
      val en = initEnders Nodes Gnew
      val Start = SetNode.union (st, SetNode.fromList (List.concat (List.map (suc Gnew) (SetNode.listItems en ))))
      val End = SetNode.union (en, SetNode.fromList (List.concat (List.map (pred Gnew) (SetNode.listItems st ))))
      (* finds all nodes in the basic block which start with s*)
      fun FindEachBlock Gnew e [s] = if (SetNode.member (e,s))
				     then [NodeMap.lookup (Vmap, s)]
				     else (NodeMap.lookup (Vmap, s))::FindEachBlock Gnew e (suc Gnew s)
	| FindEachBlock _ _ _ = []
      (* Detect all Basic Blocks*)
      fun Detect Gnew (s::ss) e = (FindEachBlock Gnew e [s])::(Detect Gnew ss e)
	| Detect Gnew [] e = nil
			       
  in
      Detect Gnew (SetNode.listItems Start) End
  end      
end
end

(* Testing using an Int graph *)
				
structure IntNode = struct
type node = int
end
structure IntGraph = Graph (IntNode)
open IntGraph
val Gnew:graph = (ref [], ref []);
newNode Gnew 1;
newNode Gnew 2;
newNode Gnew 3;
newNode Gnew 4;
newNode Gnew 5;
newNode Gnew 6;
newNode Gnew 7;
newNode Gnew 8;
newNode Gnew 9;
addEdge (1,2) Gnew;
addEdge (2,3) Gnew;
addEdge (3,4) Gnew;
addEdge (3,5) Gnew;
addEdge (5,6) Gnew;
addEdge (6,7) Gnew;
addEdge (4,8) Gnew;
addEdge (8,9) Gnew;
addEdge (9,6) Gnew;
structure B = Blockfn (IntGraph)
val Basic = B.DetectBasicBlocks Gnew;
